﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;

namespace ProjectStructureWebAPI.Repository
{
    public class UserRepository: IRepository<User>
    {
        private List<User> users = new List<User>();
        private static int startId = 1;
        public IEnumerable<User> GetItems()
        {
            return users;
        }
        public User GetSingleItem(int id)
        {
            return users.FirstOrDefault(i => i.Id == id);
        }
        public void Create(User entity)
        {
            entity.Id = startId;
            startId++;
            users.Add(entity);
        }
        public void Update(int id, User entity)
        {
            int index = users.IndexOf(users.FirstOrDefault(e => e.Id == id));
            users[index] = entity;
            users[index].Id = id;
        }
        public void Delete(int id)
        {
            users.Remove(users.FirstOrDefault(i => i.Id == id));
        }

    }
}
