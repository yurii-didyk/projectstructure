﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;

namespace ProjectStructureWebAPI.Repository
{
    public class TeamRepository: IRepository<Team>
    {
        private List<Team> teams = new List<Team>();
        private static int startId = 1;
        public IEnumerable<Team> GetItems()
        {
            return teams;
        }
        public Team GetSingleItem(int id)
        {
            return teams.FirstOrDefault(i => i.Id == id);
        }
        public void Create(Team entity)
        {
            entity.Id = startId;
            startId++;
            teams.Add(entity);
        }
        public void Update(int id, Team entity)
        {
            int index = teams.IndexOf(teams.FirstOrDefault(e => e.Id == id));
            teams[index] = entity;
            teams[index].Id = id;
        }
        public void Delete(int id)
        {
            teams.Remove(teams.FirstOrDefault(i => i.Id == id));
        }
    }
}
