﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;

namespace ProjectStructureWebAPI.Repository
{
    public class ProjectRepository: IRepository<Project>
    {
        private List<Project> projects = new List<Project>();
        private static int startId = 1;

        public IEnumerable<Project> GetItems()
        {
            return projects;
        }
        public Project GetSingleItem(int id)
        {
            return projects.FirstOrDefault(i => i.Id == id);
        }
        public void Create(Project entity)
        {
            entity.Id = startId;
            startId++;
            projects.Add(entity);
        }
        public void Update(int id, Project entity)
        {
            int index = projects.IndexOf(projects.FirstOrDefault(e => e.Id == id));
            projects[index] = entity;
            projects[index].Id = id;
        }
        public void Delete(int id)
        {
            projects.Remove(projects.FirstOrDefault(i => i.Id == id));
        }
    }
}
