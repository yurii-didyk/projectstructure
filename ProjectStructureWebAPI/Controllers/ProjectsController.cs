﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;

        }

        [HttpGet]
        public IEnumerable<ProjectDTO> GetProjects()
        {
            return _projectService.Get();

        }
        [HttpGet("{id}")]
        public ProjectDTO Get(int id)
        {
            return _projectService.GetSingle(id);
        }
        [HttpPost]
        public void Add([FromBody] ProjectDTO projectDto)
        {
            _projectService.Add(projectDto);
        }
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectService.Delete(id);
        }
        [HttpPut("{id}")]
        public void Update(int id, [FromBody] ProjectDTO projectDto)
        {
            _projectService.Update(id, projectDto);
        }

    }
}
