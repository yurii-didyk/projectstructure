﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;

        }
        [HttpGet]
        public IEnumerable<UserDTO> GetUsers()
        {
            return _userService.Get();
        }
        [HttpGet("{id}")]
        public UserDTO Get([FromBody] int id)
        {
            return _userService.GetSingle(id);
        }
        [HttpPost]
        public void Add([FromBody] UserDTO userDto)
        {
            _userService.Add(userDto);
        }
        [HttpDelete("{id}")]
        public void Delete([FromBody] int id)
        {
            _userService.Delete(id);
        }
        [HttpPut("{id}")]
        public void Update(int id, [FromBody] UserDTO userDto)
        {
            _userService.Update(id, userDto);
        }

    }
}