﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Repository;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class QueryService
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Models.Task> _taskRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IMapper _mapper;

        public QueryService(IRepository<Project> projectRepository,
            IRepository<Models.Task> taskRepository,
            IRepository<Team> teamRepository,
            IRepository<User> userRepository,
            IMapper mapper)
        {
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public IEnumerable< TaskCountForProjectDTO> TasksCountForProject(int userId)
        {
            var result = (from task in _taskRepository.GetItems()
                          group task by task.ProjectId into tasks
                          join project in _projectRepository.GetItems() on tasks.Key equals project.Id
                          where project.AuthorId == userId
                          select new TaskCountForProjectDTO
                          {
                              Project = _mapper.Map<ProjectDTO>(project),
                              TasksCount = tasks.Count()
                          });
            return result;
        }
        public List<DTOs.TaskDTO> TasksForPerformer(int userId)
        {
            var result = _taskRepository.GetItems().Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList();
            return _mapper.Map<List<DTOs.TaskDTO>>(result);
        }

        public List<Tuple<int, string>> FinishedTasksInThisYear(int userId, int year = 2019)
        {
            var result = _taskRepository.GetItems().Where(t => t.State == TaskState.Finished
                && t.FinishedAt.Year == year && t.PerformerId == userId)
                .Select(t => new Tuple<int, string>(t.Id, t.Name)).ToList();
            return result;
        }

        public List<Tuple<int, string, List<User>>> TeamsOlder12(int age = 12)
        {
            var result = (from user in _userRepository.GetItems()
                          join team in _teamRepository.GetItems() on user.TeamId equals team.Id
                          where 2019 - user.Birthday.Year > age
                          orderby user.RegisteredAt descending
                          group user by team into users
                          select new Tuple<int, string, List<User>>(users.Key.Id, users.Key.Name, users.ToList())).ToList();
            return result;
        }

        public List<Tuple<User, List<Models.Task>>> GetSortedUsers()
        {

            var result = _userRepository.GetItems().OrderBy(u => u.FirstName)
                .Select(u => new Tuple<User, List<Models.Task>>(u, _taskRepository.GetItems()
                                                                      .Where(t => t.PerformerId == u.Id)
                                                                      .OrderByDescending(y => y.Name.Length)
                                                                      .ToList())
                                                         ).ToList();
            return result;
        }

        public HelperModel1 GetModel1(int userId)
        {
            var result = (from user in _userRepository.GetItems()
                          where user.Id == userId
                          join project in _projectRepository.GetItems() on user.Id equals project.AuthorId into projects
                          join task in _taskRepository.GetItems() on projects.OrderBy(p => p.CreatedAt).FirstOrDefault().Id equals task.ProjectId into tasks

                          select new HelperModel1
                          {
                              User = user,
                              LastProject = projects.OrderBy(p => p.CreatedAt).FirstOrDefault(),
                              TasksCount = tasks.Count(),
                              CanceledAndUnfinishedTasksCount = _taskRepository.GetItems().Where(t => /* t.State == TaskStates.Canceled ||*/ t.State != TaskState.Finished && t.PerformerId == userId).Count(),
                              LongestTask = _taskRepository.GetItems().Where(t => t.PerformerId == userId).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
                          }).FirstOrDefault();

            return result;
        }

        public HelperModel2 GetModel2(int projectId)
        {
            var result = (from project in _projectRepository.GetItems()
                          where project.Id == projectId
                          join task in _taskRepository.GetItems() on project.Id equals task.ProjectId into tasks
                          select new HelperModel2
                          {
                              Project = project,
                              LongestTaskByName = tasks.OrderByDescending(t => t.Name).FirstOrDefault(),
                              LongestTaskByDescription = tasks.OrderByDescending(t => t.Description).FirstOrDefault(),
                              UsersCount = (from user in _userRepository.GetItems()
                                            where user.TeamId == project.TeamId && (project.Description.Length > 25 || tasks.Count() < 3)
                                            select user).Count()

                          }).FirstOrDefault();
            return result;

        }

    }
}
