﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class ProjectService
    {
        private readonly IRepository<Project> _repository;
        private readonly IMapper _mapper;

        public ProjectService(IRepository<Project> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Add(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _repository.Create(project);
        }
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
        public void Update(int id, ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _repository.Update(id, project);
        }
        public ProjectDTO GetSingle(int id)
        {
            var project = _repository.GetSingleItem(id);
            return _mapper.Map<ProjectDTO>(project);
        }
        public IEnumerable<ProjectDTO> Get()
        {
            var projects = _repository.GetItems();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
    }
}
