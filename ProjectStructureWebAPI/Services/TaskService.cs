﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class TaskService
    {
        private readonly IRepository<Models.Task> _repository;
        private readonly IMapper _mapper;

        public TaskService(IRepository<Models.Task> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Add(TaskDTO taskDto)
        {
            var task = _mapper.Map<Models.Task>(taskDto);
            _repository.Create(task);
        }
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
        public void Update(int id, TaskDTO taskDto)
        {
            var task = _mapper.Map<Models.Task>(taskDto);
            _repository.Update(id, task);
        }
        public TaskDTO GetSingle(int id)
        {
            var task = _repository.GetSingleItem(id);
            return _mapper.Map<TaskDTO>(task);
        }
        public IEnumerable<TaskDTO> Get()
        {
            var tasks = _repository.GetItems();
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

    }
}
