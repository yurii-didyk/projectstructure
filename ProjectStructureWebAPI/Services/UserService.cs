﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class UserService
    {
        private readonly IRepository<User> _repository;
        private readonly IMapper _mapper;

        public UserService(IRepository<User> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Add(UserDTO teamDto)
        {
            var user = _mapper.Map<User>(teamDto);
            _repository.Create(user);
        }
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
        public void Update(int id, UserDTO teamDto)
        {
            var user = _mapper.Map<User>(teamDto);
            _repository.Update(id, user);
        }
        public UserDTO GetSingle(int id)
        {
            var user = _repository.GetSingleItem(id);
            return _mapper.Map<UserDTO>(user);
        }
        public IEnumerable<UserDTO> Get()
        {
            var users = _repository.GetItems();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
    }
}