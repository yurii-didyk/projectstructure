﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class TeamService
    {
        private readonly IRepository<Team> _repository;
        private readonly IMapper _mapper;

        public TeamService(IRepository<Team> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Add(TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            _repository.Create(team);
        }
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
        public void Update(int id, TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            _repository.Update(id, team);
        }
        public TeamDTO GetSingle(int id)
        {
            var team = _repository.GetSingleItem(id);
            return _mapper.Map<TeamDTO>(team);
        }
        public IEnumerable<TeamDTO> Get()
        {
            var teams = _repository.GetItems();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
    }
}
