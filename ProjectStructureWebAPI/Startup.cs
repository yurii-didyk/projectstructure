﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Services;
using AutoMapper;

namespace ProjectStructureWebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSingleton<IRepository<Project>, ProjectRepository>();
            services.AddSingleton<IRepository<User>, UserRepository>();
            services.AddSingleton<IRepository<Models.Task>, TaskRepository>();
            services.AddSingleton<IRepository<Team>, TeamRepository>();
            services.AddScoped<ProjectService>();
            services.AddScoped<UserService>();
            services.AddScoped<TeamService>();
            services.AddScoped<QueryService>();
            services.AddScoped<TaskService>();
            var mapper = MapperConfiguration().CreateMapper();
            services.AddScoped(_ => mapper);
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Models.Project, DTOs.ProjectDTO>().ReverseMap();
                cfg.CreateMap<Models.Task, DTOs.TaskDTO>().ReverseMap();
                cfg.CreateMap<Models.User, DTOs.UserDTO>().ReverseMap();
                cfg.CreateMap<Models.Team, DTOs.TeamDTO>().ReverseMap();
            });
            return config;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
