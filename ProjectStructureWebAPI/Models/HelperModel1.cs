﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Models
{
    public class HelperModel1
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TasksCount { get; set; }
        public int CanceledAndUnfinishedTasksCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
