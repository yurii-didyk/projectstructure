﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Models
{
    public class HelperModel2
    {
        public Project Project { get; set; }
        public Task LongestTaskByName { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public int UsersCount { get; set; }
    }
}
